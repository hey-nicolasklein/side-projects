//
//  ViewController.swift
//  SickScrollPane
//
//  Created by Nicolas Klein on 20.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import UIKit

struct scrollViewDataStruct {
    let title : String?
    let image : UIImage?
}


class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var scrollViewData = [scrollViewDataStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollViewData = [scrollViewDataStruct.init(title: "First", image: #imageLiteral(resourceName: "WizArtInstagram")), scrollViewDataStruct.init(title: "Second", image: #imageLiteral(resourceName: "GzuzArtInstagram"))]
        
        scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(scrollViewData.count)
        
        var i = 0
        
        for data in scrollViewData{
            let view = CustomView(frame: CGRect(x: 10 + (self.scrollView.frame.width * CGFloat(i)), y: 0, width: self.scrollView.frame.width - 20, height: self.scrollView.frame.height))
            
            view.imageView.image = data.image
            
            self.scrollView.addSubview(view)
            
            i += 1
        }
    }
}

class CustomView: UIView{
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.darkGray
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(imageView)
        
        
        imageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
