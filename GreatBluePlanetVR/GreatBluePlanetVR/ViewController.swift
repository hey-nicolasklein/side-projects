//
//  ViewController.swift
//  GreatBluePlanetVR
//
//  Created by Nicolas Klein on 21.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let scene = SCNScene()
        
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 5)
        
        scene.rootNode.addChildNode(cameraNode)
        
        let stars = SCNParticleSystem(named: "particleSystem.scnp", inDirectory: nil)!
        scene.rootNode.addParticleSystem(stars)
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light?.type = .omni
        //TODO play with values for other results and try other lights
        //although sun isn't that not omni anyway
        lightNode.position = SCNVector3(x: 0, y: 10, z: 2)
        scene.rootNode.addChildNode(lightNode)
        
        let earthNode = EarthNode()
        scene.rootNode.addChildNode(earthNode)
        
        
        let sceneView = self.view as! SCNView
        
        sceneView.scene = scene
        
        sceneView.showsStatistics = true
        sceneView.backgroundColor = UIColor.black
        sceneView.allowsCameraControl = true
        
    }

    override var prefersStatusBarHidden: Bool{
        return true
    }
}

