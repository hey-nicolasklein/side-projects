//
//  ViewController.swift
//  EarthInYourPalmAR
//
//  Created by Nicolas Klein on 27.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var planeGeometry: SCNPlane!
    let planeIdentifier = [UUID]()
    var anchors = [ARAnchor]()
    var sceneLight: SCNLight!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // We want to use our own lighting
        sceneView.autoenablesDefaultLighting = false
        
        // Make our own light and add it to a Node
        sceneLight = SCNLight()
        sceneLight.type = .omni
        
        let lightNode = SCNNode()
        lightNode.light = sceneLight
        lightNode.position = SCNVector3(0, 10, 2)
        
        let scene = SCNScene()
        // Set the scene to the view
        sceneView.scene = scene
        
        // Adds our own lighting-Node to the scene
        sceneView.scene.rootNode.addChildNode(lightNode)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        configuration.planeDetection = .horizontal
        configuration.isLightEstimationEnabled = true

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let locationOfTouch = touch?.location(in: sceneView)
    
        let hitResults = sceneView.hitTest(locationOfTouch!, types: .featurePoint)
        
        if let hit = hitResults.first{
            let transform = hit.worldTransform
            
            let vector = SCNVector3(x: transform.columns.3.x, y: transform.columns.3.y, z: transform.columns.3.z)
            
            let earth = EarthNode()
            earth.position = vector
            
            sceneView.scene.rootNode.addChildNode(earth)
        }
    }

    // MARK: - ARSCNViewDelegate
    
//Here we can
func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        
        if let myAnchor = anchor as? ARPlaneAnchor{
            planeGeometry = SCNPlane(width: CGFloat(myAnchor.extent.x), height: CGFloat(myAnchor.extent.x))
            planeGeometry.firstMaterial?.diffuse.contents = UIColor.green.withAlphaComponent(0.5)
            let planeNode = SCNNode(geometry: planeGeometry)
            planeNode.position = SCNVector3(x: myAnchor.center.x, y:0, z: myAnchor.center.z)
            planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)
            
            node.addChildNode(planeNode)
            anchors.append(myAnchor) 
        }
     
        return node
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
