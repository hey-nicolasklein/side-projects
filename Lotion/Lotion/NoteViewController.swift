//
//  ViewController.swift
//  Lotion
//
//  Created by Nicolas Klein on 18.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import UIKit
import os.log

class NoteViewController: UIViewController, UITextViewDelegate {
    
    //MARK: Properties
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tbNote: UITextView!
    @IBOutlet weak var btnSaveNote: UIButton!
    
    @IBOutlet weak var btnDone: UIBarButtonItem!
    

    /*
     This value is either passed by the tableViewController's prepare(for: sender) method, or created here
     */
    var note: Note?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbNote.delegate = self
        
        if let note = note{
            tbNote.text = note.noteText
        }
        
        updateSaveButtonState()
    }

    //MARK: Actions
    @IBAction func setDefaultText(_ sender: UIButton) {
        tbNote.text = "Default Text"
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        //Testing if UINavigationController is what presents the view
        let presentingInAddNoteMode = presentingViewController is UINavigationController
    
        tbNote.resignFirstResponder()
        
        if presentingInAddNoteMode{
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The NoteViewController is not inside a navigation controller.")
        }
    }
    
    
    //MARK: UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        btnDone.isEnabled = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        updateSaveButtonState()
    }
    //TODO ScrollTextView when keyboard is presented
    //TODO Add top bar to save and hide keyboard
    
    //MARK: Navigation
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        //Check if the save button was pressed.
        guard let button = sender as? UIBarButtonItem, button == btnDone else{
            os_log("Save Button wasnt pressed.", log: .default, type: .debug)
            return
        }
        
        let text = tbNote.text ?? ""
        note = Note(noteText: text)
    }
    
    private func updateSaveButtonState(){
        //let text = tbNote.text ?? ""
        //btnSaveNote.isEnabled = !text.isEmpty
        btnSaveNote.isEnabled = true
    }
    
}

