//
//  NoteTableViewCell.swift
//  Lotion
//
//  Created by Nicolas Klein on 18.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    @IBOutlet weak var imgNote: UIImageView!
    @IBOutlet weak var lblNoteTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
