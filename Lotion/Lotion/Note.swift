//
//  Note.swift
//  Lotion
//
//  Created by Nicolas Klein on 18.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import Foundation
import UIKit
import os.log

class Note: NSObject, NSCoding{
    
    //MARK: Properties
    var noteText: String
    var date: Date
    
    init(noteText: String){
        self.noteText = noteText
        self.date = Date()
    }
    
    struct PropertyKey{
        static let noteText = "noteText"
    }
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("notes")
    
    //MARK: NSEncoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(noteText, forKey: PropertyKey.noteText)
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        // The name is required. If we cannot decode a name string, the initializer should fail.
        guard let noteText = aDecoder.decodeObject(forKey: PropertyKey.noteText) as? String else {
            os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        // Must call designated initializer.
        self.init(noteText: noteText)
    }
    
    
}
