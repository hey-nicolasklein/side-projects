//
//  TableViewController.swift
//  Lotion
//
//  Created by Nicolas Klein on 18.05.19.
//  Copyright © 2019 Nicolas Klein. All rights reserved.
//

import UIKit
import os.log

class TableViewController: UITableViewController {

    //MARK: Properties
    var notes = [Note]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        //LoadSampleNotes
        navigationItem.leftBarButtonItem = editButtonItem
        //loadSampleNotes()
        
        // Load any saved meals, otherwise load sample data.
        if let savedNotes = loadNotes() {
            notes += savedNotes
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "NoteTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? NoteTableViewCell else{
            fatalError("cell is not a NoteTableViewCell")
        }

        let note = notes[indexPath.row]
        cell.lblNoteTitle.text = note.noteText
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notes.remove(at: indexPath.row)
            saveNotes()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch(segue.identifier ?? ""){
        case "AddItem": os_log("Add item", log: .default, type: .debug)
        case "ShowDetail":
            guard let noteDetailViewController = segue.destination as? NoteViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedCell = sender as? NoteTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedNote = notes[indexPath.row]
            noteDetailViewController.note = selectedNote
            
            default: fatalError("unknown screen \(segue.identifier)")
        }
    }
    
    //MARK: Private methods
    
    func loadSampleNotes(){
        let note1 = Note(noteText: "Take Sara with the Bus home.")
        let note2 = Note(noteText: "Buy Milk")
        let note3 = Note(noteText: "Reserve Ticket for Super Bowl")
        
        notes += [note1,note2,note3]
    }

    //MARK: Actions
    
    @IBAction func undwindToTableView(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? NoteViewController,
            let note = sourceViewController.note{
            
            sourceViewController.tbNote.resignFirstResponder()
            
            if let indexPath = tableView.indexPathForSelectedRow{
                //update existing note
                notes[indexPath.row] = note
                tableView.reloadRows(at: [indexPath], with: .none)
            }
            else{
                //Adding a new note
                let newIndexPath = IndexPath(row: notes.count,section: 0)
                notes.append(note)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        saveNotes()
        }
    }
    private func saveNotes(){
        let isSuccessFullSave = NSKeyedArchiver.archiveRootObject(notes, toFile: Note.ArchiveURL.path)
        
        if isSuccessFullSave{
            os_log("Note successfull saved", log: OSLog.default, type: .debug)
        }
        else{
            os_log("Note not saved", log: OSLog.default, type: .debug)
        }
    }
    
    private func loadNotes() -> [Note]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Note.ArchiveURL.path) as? [Note]
    }
}
